Template for a minimal Docker container based on busybox and containing the Zig toolchain from the development branch (master).

It can be used as an image in Woodpecker for automated testing: https://ziglings.org/src/branch/main/.woodpecker/eowyn.yml

This Docker container is created every night with the latest Zig version.
